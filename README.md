# [The Exploding Greenhouse]

This is the digital garden for [MakeJames]. The source code is open, if you would like to make a suggestion or an edit, please feel free to either raise an issue or MR with your changes. I will review these when I get a chance.

The main purpose of this repo and site is as a knowledge base of my own learning and development.

## Static Site Generation

This repo uses the Go implemented static site generator [Hugo] and is built using the theme [hugo-book] by alex-shpak

### Templates

There are several page templates stored in the `archetypes/` directory that can be used with the hugo cli to template out a new page, with `hugo new --kind <archetype> <path>`

```sh
hugo new <path>               # use the default page template (creates a markdown file)
hugo new --kind branch <path> # creates a page in a directory that is rendered in the menu
hugo new --kind leaf <path>   # creates a page in a directory that is not rendered in the menu
```

All Content files are rendered from the `content/` directory

```sh
├── content
│   ├── Section
│   │   ├── _index.md      # will render the page for Section (/section)
│   │   ├── page.md        # is a page of the Section (/section/page)
│   │   ├── Sub-Section
│   │   │   ├── _index.md  # (/section/sub-section)
│   ├── _index.md          # site home (/)
```

Each Section, page and sub-section can be independently enabled as a menu item by setting `true/false` for the `bookHidden` parameter in the front matter for a page

### Shortcodes

One of the features of hugo is to call a 'shortcode' from within the content file that will render templated html. Within these templates you are able to call attributes and variables from the page or the site to generate drop-in functionality at build time.

#### Custom shortcodes

- escaped-code
- get-summary
- icon
- section

#### Theme shortcodes

Hugo-book ships with several shortcodes that are implemented in various places on this site. `{{< columns >}}` is a notable one, for building flexbox containers into the article.

Read more in the [README for the theme](https://github.com/alex-shpak/hugo-book#shortcodes)

#### Icons

There are a limited array of `svg` icons stored in the `./assets` directory. These can be retrieved in `.html` or markdown files by passing the name of the svg icon. These icons are displayed 'inline' at 85% of the line hight.

```html
<!--To add an inline icon in html-->
{{ partial "icon.html" "<name-of-icon" }}
```

```md
<!--To add an inline icon in markdown-->
{{% icon <name-of-icon> % }}
```

To add new icons to the directory, simply create the svg file with valid xml and save it to the `./assets/svg` directory


## Development

Changes should me made on a branch and a merge request raised. Site publication is triggered only on builds to `main` and is built from `main`

### Conventional commits

This project uses conventional commits and the merge request *should* detail the intention of the change. This project also uses conventional names for branches.

Commit Messages should be prefixed with a stub for the change

```sh
INIT: # structural changes to the repo
FUNC: # functional changes
DOCS: # documentation, includes content generated for the site
FIX:  # resolve an issue
LINT: # correct spelling, style or formatting errors
```

Branches should be limited to a specific change. These should be scoped in the branch name

```
docs/    # updates to supporting documentation such as READMEs
feature/ # functional changes, new layouts
fix/     # resolve a defect
content/ # changes adding or otherwise changing content (should be limited to content changes only)
```

[The Exploding Greenhouse]: https://garden.makejames.com
[MakeJames]: https://makejames.com
[Hugo]: https://gohugo.io/
[hugo-book]: https://github.com/alex-shpak/hugo-book
