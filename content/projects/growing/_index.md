---
title: Growing
createdDate: 2023-03-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Growing projects are projects that I am actively working on. Code projects will be committed to version control.

Projects that are growing do not necessarily imply age or completion.

<!--more-->

{{< section >}}
