---
title: "The Exploding Greenhouse"
createdDate: 2023-03-12
---

This is my digital garden project. Started in 2023, this project is under active development.

I started my digital garden as a means to document things I have learnt, and categorise my knowledge so that it is easily searchable.
<!--more-->

## Requirements

- It **must** be easy for me to create new content for the website
- Content **must** be version controlled
- It **must** be possible to search the digital garden
- The site **must** be W3C A compliant, Should be AA compliant and Could be AAA
- The site **should** be navigable by topic, tags and through bread-crumbed trails

## Supporting Technology

### Hugo

### Hugo-Book

### Gitlab Pages
