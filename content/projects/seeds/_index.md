---
title: Seeds
createdDate: 2023-03-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 1
---

Seed ideas are ideas that have little to no form. They may only exist as a name or a loose concept or they might have more substance.

These projects do not form a to-do list, and may never grow. These projects are however a chance to document ideas and designs.

<!--more-->

{{< section >}}
