---
title: "The Agile Manifesto"
createdDate: 2023-03-16
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

The Agile Manifesto is a set of 4 values that prioritise people, communication, and adaptability over rigid processes and procedures. The manifesto also includes 12 principles that provide more specific guidance on implementing these values.

<!--more-->

The emphasis on working software and feedback, promotes iterative, incremental development cycles with continuous customer and stakeholder feedback. This approach helps teams adapt to changing and evolving requirements quickly and effectively. In embracing this flexibility within a people centric approach, the Agile Manifesto has helped shit software development away from process heavy working practices towards being more people centric.

From this manifesto has born many frameworks and methodologies to help individuals and organisations change from more traditional project management methods often termed as Waterfall.
