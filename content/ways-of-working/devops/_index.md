---
title: "DevOps"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

DevOps is a methodology combining cultural philosophies, practices and tools designed to improve organisational software change processes. The intention of DevOps is *"to reduce the time between committing a change ... [and it] being placed into normal production"*.

<!--more-->

There is no set or uniform definition of DevOps, which has lead to individuals and organisations coining their own definitions. As a consequence the phrase is loaded and fraught with confusion.

{{< section >}}

## References

- [DevOps: Wiki](https://en.wikipedia.org/wiki/DevOps)
- [DevOps: A Software Architecht's Perspective](https://www.oreilly.com/library/view/devops-a-software/9780134049885/)

