---
title: MoSCoW
createdDate: 2023-11-25
bookHidden: true
---

MoSCoW is a prioritisation technique used in business management
or product or project management to establish shared understanding with stakeholders.
Broadly, the language of MoSCoW enables a clear short hand to effectively communicate the needs of a system.

<!--more-->

Some of the language of MoSCoW is borrowed from [RFC-2119]({{< relref "requirements" >}}),
particuarly MUST HAVE and SHOULD HAVE.

The four elements of MoSCoW are:

- Must have
- Should have
- Could have
- Won't have / Will have / Would have

## Would Have

I find the W of MoSCoW inherently problematic.
That the framework has multiple definitions poses an existential blocker to providing a shared language around requirements.
What is worse is that many dismiss the detail of the `W` requirements as the 'stuff that we won't get to'
or the 'Out of Scope' section.

To my mind calling out what won't be achieved by a particular ask is incredibly powerful.
But what is more helpful to a team, is the sense of the next horizon,
what will be asked in the future.

Whilst solutions should aim to solve the pressing problem rather than design around a hypothetical,
the reality is that (in software development) teams often need a direction of travel.
There is rarely a single solution to a problem
and having knowledge of what may be coming up can help inform the present solution.

```
Service
- Must respond to user requests
- Must provide the most up to date date
- Must be able to authenticate user requests
- Should be compliant with x standard
- Will integrate with legacy systems
```

## RFC-2119

Clearly the language of RFC-2119 is present in this framework,
but the link between the two is rarely highlighted.

## References

- [Product Plan: MoSCoW prioritisation](https://www.productplan.com/glossary/moscow-prioritization/)
- [Wiki: MoSCoW](https://en.wikipedia.org/wiki/MoSCoW_method)
