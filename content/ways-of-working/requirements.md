---
title: Requirements
createdDate: 2023-11-15
---

The language used around software development is extremely important.
The success or failure of a product can depend on how you choose to describe it.
More importantly, if there is any vagueness or room for interpretation,
these can lead to the wrong implementation and can be extremely costly to rectify.

<!--more-->

The best practice guidance leans on the language used for informational directives,
such as **MUST** and **SHOULD**.
[RFC-2119](https://www.rfc-editor.org/info/rfc2119)
outlines the best current guidelines for requirements.
This outlines two things:

- There are words that carry 'special' linguistic meaning when referring to requirements
- These words carry that meaning when they are capitalised.

The key words proposed in RFC-2119 are:

- MUST, MUST NOT, REQUIRED, SHALL, SHALL NOT
- SHOULD, SHOULD NOT, RECOMMENDED, NOT RECOMMENDED
- MAY, OPTIONAL

RFC-2119 goes further,
to specify that these imperatives MUST only be used when required for interoperation
and not used to force a particular method (for example).

## Referencing RFC-2119

Specifications such as [Semantic Line Breaks](https://sembr.org/)
or [Semantic Versioning](https://semver.org/)
reference the Request For Comment as:

```
The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document
are to be interpreted as described in RFC 2119.
```

## References

- [RFC-2119](https://www.rfc-editor.org/info/rfc2119)
