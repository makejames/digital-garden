---
title: Ways of Working
createdDate: 2023-11-12
bookFlatSection: true
bookCollapseSection: false
bookHidden: false
weight: 5
---

This section of the garden is dedicated to how people work. Collaboration, team work, empowerment, takes time and effort to understand and implement. There is no 'one-size-fits-all' solution when talking about any one person, let alone describing how many people work together.

<!--more-->

As one of those "agile enthusiasts", I am biased.

However, behind all the jargon and linked-in articles, there are elements of basic psychology.

{{< columns >}}

## [Agile]({{< relref "agile" >}})

{{< summary "ways-of-working/agile" >}}

<--->

## [Dev-Ops]({{< relref "devops" >}})

{{< summary "ways-of-working/devops" >}}

{{< /columns >}}
