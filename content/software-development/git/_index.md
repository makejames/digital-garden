---
title: "Git"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

Git is an open source version control tool that enables developers to track and manage changes within a repository. Git enables scaleable and distributed software development and is considered best practice in modern software development.

<!--more-->

Learning Git's core features and being able to navigate a Git repository is one of the first hurdles anyone starting their software development journey needs to tackle. 

{{< section >}}

## References

- [Git Home page](https://git-scm.com/)
