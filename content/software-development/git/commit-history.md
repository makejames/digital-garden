---
title: Commit History
createdDate: 2023-04-20T06:41:51Z
bookHidden: true
draft: false
weight: 5
---

The `git log` command is an extremely powerful
and useful command with in the git version control toolbox.

Used on its own, `git log` will output the commit history from your current contexts.
On its own will probably produce an output that isn't quite what you are looking for.
Fortunately, the command is extremely flexible
and the output can be tailored to display the output you need.

<!--more-->

## View changes between two commits

There are two special notations
that are useful for running comparative changes between two commits or references.

### 2 Dots - Between two commits

`<commit-1>..<commit-2>`

This is useful for viewing the commits between two references eg:

```
git log origin..HEAD     # changes since the last update to the remote
```

### 3 Dots - Symmetric Difference

`<commit-1>...<commit-2>`

This command is useful to show the commit changes between two branches.
This is particularly useful when working with multiple parallel branches,
as this output will show the order in which those changes will be applied on merge.

## Formatting

The default format is `medium` which will output the main detail of the commit:

- commit hash,
- commit message
- author
- date

I find this is often excessive for most scenarios,
especially in personal projects.
My main objection to the default medium format is
it spreads this output over multiple lines.
The history that you are looking at will likely go outside of the view window of your terminal.
Ultimately you're looking at a log,
and unless you need to full commit Hash or the author name,
this output is probably overkill.

### One line

In most instances,
where all you need is a list of commit hashes
and the commit message use:
`--format=oneline` or `--oneline`

```
$ git log --oneline -n 5
```

Which will output something like:

```sh
9b42da1 (HEAD -> content/git-log, origin/main, main) Merge branch 'feature/fix-unsafe-links' into 'main'
d1c710a FUNC: fix mailto links
67d6dc2 FUNC: add link to Ko-fi
d458201 FUNC: improve spacing between icons
e01b0c1 FUNC: add icons and links to gitlab and email
```

### Format:

If the default formats do not quite fit your personal style,
you can customise the output using a string template eg:

```
git log --format=format:"%C(auto)%h %C(green)%aN%Creset %s"
```

Which will out put something like

```sh
9b42da1 MakeJames Merge branch 'feature/fix-unsafe-links' into 'main'
d1c710a makejames FUNC: fix mailto links
67d6dc2 makejames FUNC: add link to Ko-fi
d458201 makejames FUNC: improve spacing between icons
e01b0c1 makejames FUNC: add icons and links to gitlab and email
```

### Set global default

Making any of these the default log out put is as simple as:

```
git config --global format.pretty oneline  # or the format of your choosing
```

```
git config --global format.pretty format:"%C(auto)%h %C(green)%aN%Creset %s"
```

## References

- [git log documentation](https://git-scm.com/docs/git-log)
