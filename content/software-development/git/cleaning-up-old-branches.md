---
title: Cleaning up old branches
createdDate: 2023-11-12
bookHidden: true
---

In the process of software development, branches will be created, committed to, pushed, and merged. Without housekeeping, your local (and remote) git references can get cluttered quite quickly. Fortunately git ships with multiple helper functions to help manage this, namely `prune` and `delete`.

<!--more-->

## Deleting merged branches

Deleting branches can take two forms:

- `git branch --delete old-branch` or
- `git branch --Delete old-branch`

The capitalised `--Delete` (or `-D`) flag, performs a force deletion, ignoring any checks as to whether the specific branch has been merged to [trunk]({{< ref "trunk-based-development.md" >}}). This is valuable only if you are determined to delete the work on that branch.

By and large, however, this force deletion should be avoided, as this risks accidentally deleting a branch in error. The lower case `--delete` or (`-d`) is a much safer deletion method, as it checks whether the `HEAD` of the `old-branch` has been pushed and merged into the remote branch.

{{< hint info >}}

Do **not** try to delete the branch that you are currently on. This will prompt an error `error: Cannot delete branch 'old-branch' checked out at '<repository location>'`.

You *should* checkout the `main` branch before trying to delete another branch.

{{< /hint >}}

Bear in mind, if you are in a detached HEAD state, such as in the middle of an interactive `rebase` or `merge`, you will won't be able to delete your branch.

## Deleting a remote branch

You can delete a remote branch by using `git push origin :<branch-name>`.

This will still leave the local remote tracking branch, this will need to be deleted separately.

## Cleaning up remote branches

When performing a `git fetch` you can add the `--prune` flag to remove any branches that no longer exist in the remote repository. This is an important step to carry out from time to time, because the local cache of remote branches can easily become cluttered.

You can also run `git remote prune origin` to remove references to remote branches that no longer exist.

## Suggested workflow

```sh
git switch <branch-name>
git push -u origin <branch-name>

# merge the remote branch

git switch main

# if the remote branch is auto deleted you can skip the next line
git push origin :<branch-name>

git fetch --prune
git merge origin/main main
git branch -d <branch-name>

# list all branches to confirm clean up
git branch -a
```

## Restoring a deleted branch

The `git reflog` keeps a record of the `SHA` commit references of all branches local and remote. If you accidentally delete a local branch before pruning or deleting the remote, you might be in luck!

Run `git checkout [SHA]` to get to the latest commit of your deleted branch.

Once you're at the head of your deleted branch, run `git branch <branch-name> <SHA>`

## References

- [Refine.dev - git delete remote branch and local branch](https://refine.dev/blog/git-delete-remote-branch-and-local-branch/)
