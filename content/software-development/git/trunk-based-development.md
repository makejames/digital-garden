---
title: Trunk-based Development
createdDate: 2023-11-12
bookHidden: true
---

Trunk-based Development or (TBD) puts the continuous integration of code at the heart of software development. Done well, trunk based development encourages smaller changes made more frequently.

All code changes are made to a single 'Trunk' where they are available to all developers immediately. As a method of version control, Trunk-based development aims to reduce the complexity of software changes by removing the concept of the 'long-lived' Feature branch. Working in smaller batches of code, means that developers are discouraged from making broad sweeping changes to a code base, reducing the risk of significant merge conflicts.

<!--more-->

## References

- [Atlassian - Trunk-based development](https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development)
- [Circle CI - trunk vs feature based development](https://circleci.com/blog/trunk-vs-feature-based-dev/)
- [LaunchDarkly - git branching strategies](https://launchdarkly.com/blog/git-branching-strategies-vs-trunk-based-development/)
- [Split IO - Trunk Based Development](https://www.split.io/glossary/trunk-based-development/)
