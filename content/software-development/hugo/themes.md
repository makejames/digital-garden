---
title: Themes
createdDate: 2023-03-23T17:06:05Z
bookHidden: true
draft: false
weight: 5
---

Hugo Modules control the building blocks of Hugo. Themes are one type of Hugo Modules that specifically control the look and feel of the templated site. They may bring with them their own [shortcodes]({{< ref "./shortcodes" >}}), search functionality and menu generation.

<!--more-->
