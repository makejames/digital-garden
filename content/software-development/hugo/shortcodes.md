---
title: "Shortcodes"
createdDate: 2023-03-21
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Shortcodes are the easiest way to embed HTML or templates within the content of your Hugo site, without needing to rely on embedded HTML. Beyond simply templating out a snippet of HTML, shortcodes let you interface directly with the Hugo templating engine - its API and functions and data model. This, coupled with the ability to create functions, declare variables and logic clauses, makes shortcodes an immensely powerful tool within the Hugo tool set.

Shortcodes can help you create DRY readable content for your site, whilst also making it easier to maintain the shared structure or function of the site's elements.

<!--more-->

## Getting started

Short codes can be called within your markdown file using either {{% escaped-code code="{{< [shortcode-name] 'raw string parameters' >}}" %}} or {{% escaped-code code="{{% [shortcode-name] [parameters] %}}" %}} notation. As indicated, parameters can be supplied to a shortcode using raw string literals or by passing a named parameter such as `code=""`

### Creating your own

Shortcodes exist in `.html` files in the `layouts/shortcodes` directory.

```
├── layouts
│   ├── shortcodes
│   │   ├── your-custom-shortcode.html
```

From there you need to create your shortcode, the name of the file will what you call in your content. Html should be written as html, and Go templating written in `{{ }}`

Some examples from this site:

{{< tabs "uniqueid" >}}
{{< tab "section.html" >}}

Here this short code can be called from the content page where it will access the .Pages attribute of the .Page data which lists all of the 'child' pages of the page.

With each of those pages it extracts the title and summary for each page, calling the `div class` declared in the theme.

```
{{ range .Page.Pages }}
  <h3>
      <a href="{{ .RelPermalink }}">{{ .Title }}</a>
  </h3>
  <div class="markdown-inner">
    {{ default .Summary .Description }}
  </div>
{{ end }}
```
{{< /tab >}}
{{< tab "escaped-code.html" >}}

.Get fetches the code snippet passed to the code variable and renders it within a code block
```
{{ with .Get "code" }}<code>{{ . }}</code>{{ end }}
```
{{< /tab >}}
{{< /tabs >}}

## Limitations

{{< hint danger >}}
When creating shortcodes it is important to consider how `newline` characters will affect the render of the page. Hugo will interpret newline characters as an indication that there should be a new `<p>` tag which might result in interesting layouts. Simple things like your editor creating an End of Line on save can be irritating to debug.
{{< /hint >}}

Shortcodes will not work in template files. If you need this kind of functionality in a page template, you should look to partials.

As with many templating features of Hugo, be mindful of the names of the shortcodes that have been created by any themes or modules used in your site. Hugo will prioritise files in your main project directory, over that of your dependant packages, so if you want to avoid unintentional or breaking changes, be mindful of your shortcode naming choices.

## Further reading

If this is your introduction to Hugo, it is worth familiarising yourself with some of the base concepts within the Hugo templating tools such as [Variables](https://gohugo.io/variables/) and [Functions](https://gohugo.io/functions/).

## References

- [Hugo Shortcode docs](https://gohugo.io/content-management/shortcodes/)
- [Kubernetes docs - Hugo Shortcodes](https://kubernetes.io/docs/contribute/style/hugo-shortcodes/)
