---
title: "Hugo"
createdDate: 2023-03-21
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Written in Go, Hugo is a static site generator that templates static pages on the fly. It is fast and easily extensible.

<!--more-->

{{< section >}}

## References

- [Hugo home](https://gohugo.io/)
- [Hugo Docs](https://gohugo.io/documentation/)
- [Hugo Wikipedia](https://en.wikipedia.org/wiki/Hugo_(software))
- [Hugo release notes](https://github.com/gohugoio/hugo/tags)
