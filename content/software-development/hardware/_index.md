---
title: "Hardware"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

I run some of my applications on my own local network. I have a small collection of Raspberry Pi's currently running a headless version of Debian.

<!--more-->

{{< section >}}
