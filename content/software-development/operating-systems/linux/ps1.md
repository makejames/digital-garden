---
title: PS1
createdDate: 2023-11-25
bookHidden: true
---

`PS1` is a linux environment variable tat refers to *Prompt String 1*
and is usually set in a users `.bashrc`.

*Prompt String 1* refers to the string of characters that you see on the command prompt.
There are other prompt strings,
allowing the user to customise different aspects of the shell command prompt.
The other prompts are PS2, PS3, PS4.

<!--more-->

## PS1

The default, Debian PS1 looks something like this:

```sh
PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
```

However, it is also possible to embed ascii colour codes into the prompt,

```sh
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[38;05;240m\] $(parse_git_branch)\[\033[00m\]\$ '
````

## Control Commands

PS1 can use the following options to make the PS1 prompt more meaningful.

```
    d - the date in "Weekday Month Date" format (e.g., "Tue May 26")

    e - an ASCII escape character (033)

    h - the hostname up to the first .

    H - the full hostname

    j - the number of jobs currently run in background

    l - the basename of the shells terminal device name

    n - newline

    r - carriage return

    s - the name of the shell, the basename of $0 (the portion following the final slash)

    t - the current time in 24-hour HH:MM:SS format

    T - the current time in 12-hour HH:MM:SS format

    @ - the current time in 12-hour am/pm format

    A - the current time in 24-hour HH:MM format

    u - the username of the current user

    v - the version of bash (e.g., 4.00)

    V - the release of bash, version + patch level (e.g., 4.00.0)

    w - Complete path of current working directory

    W - the basename of the current working directory

    ! - the history number of this command

    # - the command number of this command

    $ - if the effective UID is 0, a #, otherwise a $

    nnn - the character corresponding to the octal number nnn

    \ - a backslash
