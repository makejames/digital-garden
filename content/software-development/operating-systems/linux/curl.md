---
title: Curl
created: 2024-01-06
bookHidden: true
---

The linux curl command enables command line http requests.

<!--more-->

## Request Methods,

You can change the method of a request by using the `-X` or `--request` flags.
This should be followed by the method verb such as `GET` or `POST`

This request *only* changes the text in the outgoing request,
there is no change to behaviour.

For more info check the
[curl documentation](https://everything.curl.dev/http/requests/method).

## Referencesa

- [curl docs](https://everything.curl.dev)
