---
title: "Windows"
createdDate: 2023-03-21
bookFlatSection: false
bookCollapseSection: false
bookHidden: true
weight: 5
---

Windows is a proprietary operating system built by Microsoft. Windows has many difference between it and its UNIX counterparts, particularly in how it handles system calls and memory management. Windows also has a permission-based system, where users and groups are assigned specific permissions to access files and resources.

Windows is actively developing tooling that provides a Linux compatibility layer that allows a user to install and run Linux distributions natively on top of the Windows kernel.

<!--more-->

{{< section >}}
