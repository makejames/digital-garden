---
title: K3s
createdDate: 2023-06-17T08:21:30Z
bookHidden: true
draft: false
weight: 5
---

Is a lightweight production ready Kubernetes.

<!--more-->

## Installation

### New Server node

```sh
curl -sfL https://get.k3s.io | sh -
```

### Agent nodes

Can be installed by passing parameters to the install script. Crucially, `K3S_URL` and `K3S_TOKEN`

```sh
curl -sfL https://get.k3s.io | K3S_URL=https://myserver:6443 K3S_TOKEN=mynodetoken sh -
```

The `K3S_URL` parameter can be established by either specifying the hostname or the ip address of the master node.

The value of the `K3S_TOKEN` can be found at `/var/lib/rancher/k3s/server/node-token` on your server node

{{< hint info >}}
**Note**
Each new node must have a unique hostname. If a unique hostname isn't available you can pass a value to `K3S_NODE_NAME`
{{< /hint >}}

## Uninstall

{{< hint warning >}}
**WARNING**
Uninstalling K3s deletes the local cluster data, configuration, and all of the scripts and CLI tools.
It does not remove any data from external datastores, or created by pods using external Kubernetes storage volumes.
{{< /hint >}}

### Server node

To uninstall K3s from a server node run

```sh
/usr/local/bin/k3s-uninstall.sh
```

### Agent node

To uninstall K3s from an agent node run

```sh
/usr/local/bin/k3s-agent-uninstall.sh
```

## References

- [Quick Start guide](https://docs.k3s.io/quick-start)
