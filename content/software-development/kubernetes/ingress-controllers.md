---
title: Ingress Controllers
createdDate: 2023-12-30
bookHidden: true
---

Ingress Controllers act as reverse proxies and load balancers for a cluster.
Adding a layer of abstraction to traffic routing,
Ingress Controllers can be configured to accept
and direct traffic to the cluster.

Ingress Controllers control the routing of external traffic
to services within the cluster.
This is achieved by exposing standard HTTP/HTTPS ports
and resolving the requested routes.

## Disabling Traefik on K3s

By default K3s ships with and installs Traefik as an ingress controller.
Traefik has a modern but relatively simple feature set,
however, it is less mature and well documented than others.

To remove Traefik from an already running k3s intance.

```sh
sudo rm -rf /var/lib/rancher/k3s/server/manifests/traefik.yaml
helm uninstall traefik traefik-crd -n kube-system
sudo systemctl restart k3s
```

## Multiple controllers

It is possible to deploy many ingress controllers to a cluster.
This can be helpful in scenarios where you want to host internal
and external services on the one cluster.

## References

- [Remove Traefik](https://qdnqn.com/k3s-remove-traefik/)
