---
title: Kubernetes
createdDate: 2023-06-17T08:15:34Z
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

Kubernetes is an open source platform for managing containerised workloads and services.

<!--more-->

{{< section >}}

## References

- [Kubernetes Overview](https://kubernetes.io/docs/concepts/overview/)
