---
title: "Vim"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

[Vim](https://github.com/vim/vim) is a fantastic code editor. It is fast, convenient, and extremely portable. As a convert from more bloated code editors like [VSCode](https://code.visualstudio.com/), [Atom](https://en.wikipedia.org/wiki/Atom_(text_editor)) or [Sublime](https://www.sublimetext.com/), I have found (following a steep learning curve) vim to be the superior editor.

<!--more-->

I use vim as my text editor of choice for code projects but I also use it to write and edit most other simple text files including `csv`, `txt` and even `emails`.

Vim ships with a built in terminal emulator, so you can build, run and test your code all without leaving the vim buffer

Beyond its out-of-the box utility and speed Vim comes with a healthy community of support and plug-ins expanding on the base utility of Vim.

{{< section >}}

## References

- [Vim repository](https://github.com/vim/vim)
