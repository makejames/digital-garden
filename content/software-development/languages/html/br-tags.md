---
title: The Line Break Element
createdDate: 2023-12-02
bookHidden: true
---

The `<br>` HTML element produces a line break in text.
This element should *only* be used where the line break is significant.

The element is problematic as it causes issues with screen readers.
By and large, strict line breaks and formatting of text should be managed by
[CSS]({{< relref "css" >}}).

<!--more-->

## Uses

The element adds a single line break to the page.
This is specifically useful for addresses:

```html
 <address>
Written by <a href="mailto:webmaster@example.com">Jon Doe</a>.<br>
Visit us at:<br>
Example.com<br>
Box 564, Disneyland<br>
USA
</address>
```

It can also be used within paragraphs where the line breaks provide additional meaning,
such as in poems:

```html
 <p>Be not afraid of greatness.<br>
Some are born great,<br>
some achieve greatness,<br>
and others have greatness thrust upon them.</p>

<p><em>-William Shakespeare</em></p> 
```

## Paragraphs

Using the `<br>` element to declare separate paragraphs is bad practice.
It creates specific accessibility issues for users of screen readers
as the tag is often declared as a structural element by the screen reader.
This can make the parsing of text unnecessarily hard for users of this technology.

Individual paragraphs should be wrapped in `<p>` tags
and additional spacing should be added using CSS.

## References

- [Mozilla - br elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/br)
- [W3C - br tag](https://www.w3schools.com/tags/tag_br.asp)
