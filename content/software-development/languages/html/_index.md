---
title: HTML
createdDate: 2023-11-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

**HyperText Markup Language** is the standard markup format for documents that are designed to be displayed in a web browser. HTML defines the semantic structure of the document. If a document requires auxiliary resources such as style definitions or scripts, HTML will define where the web browser should expect to retrieve these files from and how they affect the elements on the page.

Web browsers are able to receive HTML files from local or remote servers and render the document.

<!--more-->

{{< section >}}

## References

- [Wiki HTML](https://en.wikipedia.org/wiki/HTML)
