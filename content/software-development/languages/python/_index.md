---
title: "Python"
createdDate: 2023-03-14
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

Python is a high-level general purpose programming language. It is a feature rich, mature language that enables the development of scripts and programmes using an accessible and easily readable language

<!--more-->

{{< section >}}

## References

- [Python.org](https://www.python.org/)
- [Wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language))
