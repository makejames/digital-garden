---
title: "Languages"
createdDate: 2023-03-13
bookFlatSection: false
bookCollapseSection: true
bookHidden: false
weight: 5
---

Computer languages are defined formal languages used to communicate with computers.

<!--more-->

{{< section >}}
