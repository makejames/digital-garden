---
title: XML
createdDate: 2023-11-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

**Extensible Markup Language** (**XML**) is a markup language that syntactically defines and structures arbitrary data. XML is now in common use across the internet for the exchange of data and there are now hundreds of standards, file specifications and formats that have been developed using the XML syntax.

<!--more-->

{{< section >}}

## References

- [Wiki XML](https://en.wikipedia.org/wiki/XML)
