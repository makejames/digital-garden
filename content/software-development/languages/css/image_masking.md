---
title: CSS Image Masking
createdDate: 2023-11-11
bookHidden: true
weight: 5
---

With the CSS `mask-image` property you can create a mask to place over an element to partially or fully hide a portion of it.

<!--more-->

## Browser support

Most browsers only have partial support for CSS masking so you should use the `-webkit-` prefix in addition to the standard property. For up to date guidance on supported browsers [check the Mozilla definition of the property](https://developer.mozilla.org/en-US/docs/Web/CSS/mask#browser_compatibility)

## Use an image as a mask

You can use the `url()` value of a `.png` or `.svg` file to mask a background image. The image must have a transparent or partially transparent area, where the black area of the image represents 100% transparency.

{{< tabs >}}

{{< tab "html" >}}

```html
    <img src="img/example-image.jpeg" alt="Logo">
```

{{< /tab >}}
{{< tab "css" >}}

```css
img {
  -webkit-mask-image: url(svg/logo.svg);
  mask-image: url(svg/logo.svg);
}
```

{{< /tab >}}

{{< /tabs >}}

## Dynamically changing the colour of an SVG

`.svg` files define the fill of an element within the `xml` of the `.svg` file. This can be removed and the colour of the element specified in the styling of the particular element within the web page.

Occasionally, this does not satisfy the needs of the page. Using the `mask-image` property you can define the colour of the 'background' image and apply the mask on top. This can make it easier to manipulate any colour changing elements.

Empty `src` attributes are not valid html. To by pass this, you can use `//:0` to target a valid url that does not (and cannot) exist. This url, represents:
- `//` - the leading protocol is intentionally blank
- `:0` - port 0 does not exist

{{< tabs "dynamic" >}}

{{< tab "html" >}}

```html
    <img src="//:0" alt="Logo">
```

{{< /tab >}}
{{< tab "css" >}}

```css
img {
  background-color: white;
  -webkit-mask-image: url(svg/logo.svg);
  mask-image: url(svg/logo.svg);
}
```

{{< /tab >}}

{{< /tabs >}}

## References

- [Mozilla mask- css property](https://developer.mozilla.org/en-US/docs/Web/CSS/mask)
- [W3C example](https://www.w3schools.com/css/css3_masking.asp)
