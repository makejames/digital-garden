---
title: CSS
createdDate: 2023-11-11
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

**CSS** or **Cascading Style Sheets** is a language used to define the presentation of documents written in markup language such as [HTML]({{< ref "HTML" >}} "HTML" ) or [XML]({{< ref "xml" >}} "XML" )

<!--more-->

{{< section >}}

## References

- [CSS Tricks](https://css-tricks.com/)
