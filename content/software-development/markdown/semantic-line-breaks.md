---
title: Semantic Line Breaks
createdDate: 2024-11-27
bookHidden: true
---

In any markup language, line breaks pose an almost existential issue.
Prose in source code often leads to long continuous lines of text.
This can be extremely difficult to scan or read quickly.
Some editors bypass this issue by implementing a line wrap feature.

Semantic line breaks describes a set of principles
that can be used to help manage prose in source code.

<!--more-->

{{< hint "info" >}}

***Hints for Preparing Documents***

*Most documents go through several versions (always more than you expectedi)*
*before they are finally finished.*
*Accordingly, you should do whatever possible to make the job of changing them easy.*

*First, when you do the purely mechanical operations of typing,*
*type so subsequent editing will be easy.*
*Start each sentence on a new line.*
*Make lines short,*
*and break lines at natural places,*
*such as after commas and semicolons,*
*rather than randomly.*
*Since most people change documents by rewriting phrases and adding,*
*deleting and rearranging sentences,*
*these precautions simplify any editing you have to do later.*

— Brian W. Kernighan, 1974

{{< /hint >}}


## Context

Most lightweight markup languages join consecutive lines of text with a space.
Which gives you freedom to add line breaks within prose
without affecting the rendered output.

This makes working with text files and text in source code far easier.
Editors are very good at navigating between lines
and generally less good at navigating along long lines of text.

Once committed to version control,
it becomes much easier to track and manage changes to text.
Just because a particular editor is good at wrapping lines of text
doesn't mean that you should.

If you are writing plain text paragraphs on a single line,
you are far more likely to come across merge conflicts in your VCS
([Version-Control System]({{< relref "git" >}})).
Not to mention, your the diffs on a merge will be much easier to review!

## Principles

A SLB *MUST NOT* alter the final rendered output of a document.

A SLB *SHOULD NOT* alter the intended meaning of the text.

A SLB *MUST* occur after a sentence,
as punctuated by a period (**.**),
exclamation mark (**!**),
or question mark (**?**).

A SLB *SHOULD* occur after an independent clause,
as punctuated by a comma (**,**),
semi-colon  (**;**),
colon (**:**),
or em dash (**--**).

A SLB *MAY* occur after a dependent clause
in order to clarify grammatical structure
or satisfy line break constraints.

A SLB is *RECOMMENDED* before an enumerated or itemised list.

A SLB *MAY* be used after one or more items in a list
in order to logically group related items
or satisfy line length constraints.

A SLB *MUST NOT* occur within a hyphenated word.

A SLB *MAY* occur before and after a hyperlink.

A SLB *MAY* occur before in-line markup.

A maximum line length of 80 characters is *RECOMMENDED*.

A line *MAY* exceed the maximum line length if necessary,
such as to accommodate hyperlinks,
code elements,
or other mark-up.

## Further notes

The standard suggests that migration to `sembr` should be done gradually
for any new or revised text.

New lines can be forced with the
[`<br/>` element]({{< relref "br-tags" >}})
or by adding an additional line spacing.

Passing the `--word-diff` flag on `git diff`
can make it easier to see textual changes in source files.

## References

- [Sematic Line Breaks](https://sembr.org/)
- [Brandon Rhodes - Semantic Linefeeds](https://rhodesmill.org/brandon/2012/one-sentence-per-line/)
- [Brian W. Kernighan - UNIX for Beginners](https://web.archive.org/web/20130108163017if_/http://miffy.tom-yam.or.jp:80/2238/ref/beg.pdf)
- [Fluid Attacks writing guidelines](https://docs.fluidattacks.com/development/writing/slb/)
