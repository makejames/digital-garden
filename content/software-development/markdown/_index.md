---
title: Markdown
createdDate: 2023-11-26
bookHidden: false
---

Markdown is a plain-text formatting syntax
that makes it easy to write well structured readable plain text files.
It has become one of the web's most popular frameworks for writing plain text files.

<!--more-->

Like many markup languages, Markdown supports
[Semantic Line Breaks]({{< relref "semantic-line-breaks" >}}).

## About

Version 1.0.1 was released in 2004 by John Gruber as both a syntax
and a text-to-HTML conversion tool written in perl.
The syntax is designed to be readable and unobtrusive.

## Benefits of Markdown

Markdown is an extremely portable syntax.
The resulting plain text file can be opened by almost any editor on almost any platform.
The syntax also supports many online content generation tools,
like writing an article or adding a comment.

{{< hint "danger" >}}

This article is a draft.

{{< /hint >}}

## References

- [John Gruber's Markdown](https://daringfireball.net/projects/markdown/)
