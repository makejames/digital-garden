---
title: Cluster Management
createdDate: 2024-09-04
bookHidden: true
weight: 5
---

Cluster and Zone management is the main strength of Garage.
Garage can be run on a single device,
but the main benefits of a tool like Garage come in the replication
and redundancy.

<!--more-->

The Garage notes recommend at least 3 nodes
and set-up 3 way replication of data.
The most secure set-ups will have redundancy across 'zones'.

## Zones

In traditional networking,
a zone is a configurable boundary that you can use to grant or restrict access.
This access is based on the IP addresses of the devices concerned.
Zones can be defined by specifying one or more IP addresses
or geographic location.

In this context,
zones should be considered based on a logical organisation of devices.
This might be by their physical location or subnet.

## Initial set-up

Ensure garage is [deployed on all nodes](/software-development/garage/deployment).

### Set a common rpc_secret

For a node to be be able to connect to other nodes in a cluster,
they must share a `rpc_secret`.
This secret should be considered secret and stored securely.

### Set the rpc_public_addr

Set the `rpc_public_addr` to the public ip address of the device.

This is optional, but it can aid discovery later down the line.

{{< hint info >}}
Remember that when modifying the config file in `/etc/garage.toml`,
you will need to run `systemctl restart garage` for the changes to take effect.
{{< /hint >}}

## Configuring the cluster

Once the devices are running with the same `rpc_secret`,
the nodes in the cluster need to be connected.

### Acquire the id of each node

Each node has an id.
Run `garage node id` to print the id and connection string to console.

Output should look something like this:

```sh
<hex code id>@<device ip>:3901
```

### Connect the nodes together

On a node run `garage node connect <id>@<devide ip>:3901` for each node in the cluster.

### Garage Layout.

Running `garage status` should now list each of the nodes connected in the cluster.
To configure the cluster a 'layout' needs to be applied.

Cluster layout changes occur in two stages:

- A staging set of changes that can be previewed.
- The applying of a new version of the layout.

At any stage in this process, the existing layout
and pending layout can be viewed by running `garage layout show`

### Capacity and Zones

For each node in the cluster a zone and capacity must be set.
`garage layout assign <id> -z <zone id> -c 1T`

**Zone**

Zone is a logical or human readable reference to a zone.
This should reflect your decisions around cluster structure.

**Capacity**

Capacity is a reflection of bytes available in a cluster.

- 250GB: 250G
- 2.5TB: 2.5T

## Gateway Configuration

It is possible to configure a reverse proxy as a load balancer.
This has some merits.
It can reduce network latency and improve server management
to provision a gateway node.
Gateway nodes do not store data on them,
but are aware of the structure of the cluster.
Instead of randomly querying a node,
the gateway node knows which node to query.

This simplifies security and improves server maintenance.

Simply pass the `--gateway` flag to the `layout`command:
`garage layout assign <node id> --gateway --tag gw1 -z <zone>`

Gateway nodes do not have a capacity value.

## References

- https://garagehq.deuxfleurs.fr/documentation/cookbook/real-world/
