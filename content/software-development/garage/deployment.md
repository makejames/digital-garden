---
title: Deploying Garage
createdDate: 2024-09-04
bookHidden: true
---

There are relatively few steps to install and enable garage.
Pre-compiled binaries are available for arm and x86 architectures.
Check out the [releases page](https://garagehq.deuxfleurs.fr/download/)
for the latest release.

<!--more-->

```sh
wget https://garagehq.deuxfleurs.fr/_releases/v1.0.0/aarch64-unknown-linux-musl/garage
sudo chmod +x ./garage
sudo cp ./garage /usr/local/bin/garage
```

## Configuration

The [Quick Start Guide](https://garagehq.deuxfleurs.fr/documentation/quick-start/)
outlines how to generate a secure first configuration guide.
There are several fields that will need to be changed if deploying to a cluster.

The default path that garage checks for configuration files is
`/etc/garage.toml`.
If using  different path,
pass `-c /path/to/garage.toml` in the start up command.

To check everything is ok, run `garage server`.
This will spin up the server and you should get a healthy log output.

## Systemd Installation

Garage docs outline a pretty comprehensive [cookbook](https://garagehq.deuxfleurs.fr/documentation/cookbook/systemd/)
for deploying with systemd.

Running with systemd means runing garage as a dynamic user.
This is more secure but there are several things to note.

### Interacting with garage.

To interact with garage you will need to be a sudoer or run with sudo commands.
Checking the health of the service
or performing admin actions will require admin permissions.

### Updating `/etc/garage.toml`

The only path that the service will be able to write to is `/var/lib/garage`.

The `metadata_dir` and `data_dir` must be set in the config file as follows:

```toml
metadata_dir = /var/lib/garage/meta
data_dir = /var/lib/garage/data
```

## References

- [Quick Start](https://garagehq.deuxfleurs.fr/documentation/quick-start/)
- [Systemd Cookbook](https://garagehq.deuxfleurs.fr/documentation/cookbook/systemd/)
