---
title: "Garage"
createdDate: 2024-09-04
bookFlatSection: false
bookCollapseSection: false
bookHidden: false
weight: 5
---

[Garage](https://garagehq.deuxfleurs.fr) is a lightweight s3 compatible object store.
Designed to run on minimal specification hardware,
Garage presents an exciting option for homelab application storage.

<!--more-->

The single compiled binary makes garage simple to deploy
and without dependencies.

{{< section >}}

## References

- [Garage home page](https://garagehq.deuxfleurs.fr)
