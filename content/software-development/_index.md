---
title: "Software Development"
createdDate: 2023-03-13
bookFlatSection: true
bookCollapseSection: false
weight: 5
customTOC:
  - Title: Operating Systems
  - Title: Git
  - Title: Vim
  - Title: Markdown
  - Title: Languages
  - Title: Kubernetes
  - Title: Hardware
  - Title: Hugo
---

I spend a lot of my free time learning, tinkering and creating.
Many of these projects involve some aspect of computing.
I also work as an Agile Delivery Manager in the Public Sector.

On this branch of the garden I talk all things software;
from Agile and Lean Practices,
to Operating Systems and distributed compute.

<!--more-->

## [Operating Systems]({{< relref "operating-systems" >}})

{{< summary "software-development/operating-systems" >}}

{{< columns >}}

## [Git]({{< relref "git" >}})

{{< summary "software-development/git" >}}

<--->

## [Vim]({{< relref "vim" >}})

{{< summary "software-development/vim" >}}

{{< /columns >}}

## [Markdown]({{< relref "markdown" >}})

{{< summary "markdown" >}}

{{< columns >}}

## [Languages]({{< relref "languages" >}})

{{< summary "software-development/languages" >}}

<--->

## [Kubernetes]({{< relref "kubernetes" >}})

{{< summary "software-development/kubernetes" >}}

{{< /columns >}}


{{< columns >}}

## [Hardware]({{< relref "hardware" >}})

{{< summary "software-development/hardware" >}}

<--->

## [Hugo]({{< relref "hugo" >}})

{{< summary "software-development/hugo" >}}

{{< /columns >}}
